**Knowledge APP - Ionic 4 + Angular ❤**

---

## How to install and start debug mode

Follow the steps below:

1. Clones the repository
2. Run 'npm i' or 'npm install' to install package.json depedencies
3. Start project with 'ionic serve'

---

More details:
https://ionicframework.com/docs/installation/cli